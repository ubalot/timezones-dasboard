package main

import (
	"fmt"
	"os"
	"strings"
	"time"
)

const timeDateFormat = "2006-01-02 15:04:05 -0700 MST"

var cities = Cities{
	{name: "Rome", state: "Italy", timezone: "Europe/Rome"},
	{name: "New York", state: "New York (USA)", timezone: "America/New_York"},
	{name: "Houston", state: "Texas (USA)", timezone: "America/Chicago"},
	{name: "Bangalore", state: "India", timezone: "Asia/Kolkata"},
	{name: "Mexico City", state: "Mexico", timezone: "America/Mexico_City"},
}

const (
	NoError = iota
	ErrWrongUsage
	ErrWrongArguments
	ErrTimezoneNotFound
)

func getIntroText(t time.Time) string {
	var isDST string
	if t.IsDST() {
		isDST = "yes"
	} else {
		isDST = "no"
	}
	lines := []string{
		"utc:   " + t.UTC().Format(timeDateFormat),
		"local: " + t.Local().Format(timeDateFormat),
		"is daylight saving on? " + isDST,
		"",
	}
	return strings.Join(lines, "\n")
}

func loadLocation(tz string) *time.Location {
	loc, err := time.LoadLocation(tz)
	if err != nil {
		fmt.Println(err)
		os.Exit(ErrTimezoneNotFound)
	}
	return loc
}

// convert UTC time `t` as if we were in timezone `tz`
func convertUtcToTimezone(t time.Time, tz *time.Location) time.Time {
	targetTime := t.In(tz)
	_, offset := targetTime.Zone()
	return targetTime.Add(time.Duration(-offset * 1e9))
}

type City struct {
	name     string
	state    string
	timezone string
}

func (city *City) Convert(t time.Time) time.Time {
	tz := loadLocation(city.timezone)
	td := t.In(tz)
	return td
}

func (city *City) PrettyText(targetTime time.Time) string {
	t := city.Convert(targetTime)
	lines := []string{
		"city:      " + city.name,
		"location:  " + t.Location().String(),
		"time:      " + t.Format(timeDateFormat),
	}
	return strings.Join(lines, "\n")
}

type Cities []City

func (cities Cities) PrettyText(targetTime time.Time) string {
	blocks := []string{}
	for _, city := range cities {
		text := city.PrettyText(targetTime)
		blocks = append(blocks, text)
	}
	return strings.Join(blocks, "\n\n")
}

func handleArgs() (inputTime time.Time, inputTimezone *time.Location) {
	inputTimezone, _ = time.LoadLocation("UTC")
	nArgs := len(os.Args)
	if nArgs == 1 {
		inputTime = time.Now()
	} else if nArgs == 2 || nArgs == 3 {
		datetime := os.Args[1]
		const format = "2006-01-02 15:04"
		t, err := time.Parse(format, datetime)
		if err != nil {
			fmt.Println("For input use format: " + format)
			fmt.Println(err)
			os.Exit(ErrWrongArguments)
		}
		inputTime = t

		if nArgs == 3 {
			timezone := os.Args[2]
			if !strings.Contains(timezone, "/") {
				fmt.Println("Not valid timezone")
				os.Exit(ErrWrongArguments)
			}

			inputTimezone, err = time.LoadLocation(timezone)
			if err != nil {
				fmt.Println(err)
				os.Exit(ErrWrongArguments)
			}
		}
	} else {
		fmt.Println("Usage: " + os.Args[0] + " datetime [timezone]")
		os.Exit(ErrWrongUsage)
	}
	return
}

func main() {
	inputTime, inputTimezone := handleArgs()

	targetTime := convertUtcToTimezone(inputTime, inputTimezone)

	fmt.Println(strings.Join([]string{
		getIntroText(targetTime),
		cities.PrettyText(targetTime),
	}, "\n"))
}
