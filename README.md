# Timezone dashboard

### Run the project
Run with no arguments for current local time
```bash
go run main.go
```

Run with desired local time as first argument
```bash
go run main.go "2021-10-30 10:00"
```

Run with desired time in choosen timezone
```bash
go run main.go "2021-10-30 10:00" Europe/Berlin
```


### Run tests
```bash
go test -v
```
