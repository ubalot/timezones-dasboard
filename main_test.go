package main

import (
	"testing"
	"time"
)

func TestConvertUtcToTimezone(t *testing.T) {
	locations := []string{
		"Europe/London",
		"Europe/Rome",
		"America/New_York",
		"America/Chicago",
		"Asia/Kolkata",
		"America/Mexico_City",
	}
	for _, loc := range locations {
		datetime := time.Date(2009, 11, 17, 20, 34, 58, 651387237, time.UTC)
		tz, err := time.LoadLocation(loc)
		if err != nil {
			t.Errorf("Location doesn't exists: %s", err)
		}
		target := convertUtcToTimezone(datetime, tz)
		expected := target.In(tz)
		if target != expected {
			t.Errorf("%s is not %s in %s", target, expected, tz)
		}
	}
}
