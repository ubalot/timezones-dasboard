let
  pkgs = import <nixpkgs> {};
in pkgs.mkShell {
  packages = [
    (with pkgs; [
      go
      go-outline
      gopls
      gopkgs
      go-tools
      delve
    ])
  ];
}
